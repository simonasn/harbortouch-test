const request = require('supertest');
const { expect } = require('chai');
const app = require('../../app/server/express');
const userService = require('../../app/server/entity/user/user-service');
const blogService = require('../../app/server/entity/blog/blog-service');

describe('blogs', () => {
	const blog = {
		subject: 'testing',
		text: 'A test blog',
	};

	const updatedBlog = {
		subject: 'A new subject',
		text: 'A new text',
	};

	let token;

	/**
	 * Create a user and set the token
	 *
	 * @returns {Promise} a promise which resolves when the token is set
	 */
	function createUser() {
		return userService.createUser({ username: 'test', password: 'test'})
			.then(() => {}, () => {})
			.then(() => {
				return request(app)
					.post('/api/authenticate')
					.send({ username: 'test', password: 'test' })
					.then((response) => {
						token = response.body.token;
					});
			});
	}

	/**
	 * Creates a blog from blogData
	 *
	 * @param {Object} blogData the data of the blog to create
	 * @returns {Promise} a promise which resolves with the saved blog
	 */
	function createBlog(blogData) {
		return request(app)
			.post('/api/blogs')
			.set('Authorization', `Bearer ${token}`)
			.send(blogData)
			.then((response) => response.body);
	}

	before((done) => {
		createUser().then(() => done());
	});

	after((done) => {
		userService.deleteAll().then(() => done());
	});

	afterEach((done) => {
		blogService.deleteAll().then(done());
	});

	describe('POST /api/blogs', () => {
		it('should create a blog', (done) => {
			request(app)
				.post('/api/blogs')
				.set('Authorization', `Bearer ${token}`)
				.send(blog)
				.then((response) => {
					expect(response.statusCode).to.equal(200);
					const savedBlog = response.body;
					expect(savedBlog.id).to.not.be.null;
					done();
				});
		});
	});

	describe('PUT /api/blogs', () => {
		it('should update the blog', (done) => {
			createBlog(blog)
				.then((createdBlog) => {
					request(app)
						.put(`/api/blogs/${createdBlog.id}`)
						.set('Authorization', `Bearer ${token}`)
						.send(updatedBlog)
						.then((response) => {
							expect(response.statusCode).to.equal(200);
							const savedBlog = response.body;
							expect(savedBlog.subject).to.be.equal(updatedBlog.subject);
							expect(savedBlog.text).to.be.equal(updatedBlog.text);
							done();
						});
				});
		});
	});

	describe('GET /api/blogs', () => {
		it('return an empty list', (done) => {
			request(app)
				.get('/api/blogs')
				.set('Accept', 'application/json')
				.then((response) => {
					expect(response.statusCode).to.equal(200);
					const blogs = response.body;
					expect(blogs).to.be.empty;
					done();
				});
		});

		it('return a list with 1 item', (done) => {
			createBlog(blog)
				.then(() => {
					request(app)
						.get('/api/blogs')
						.set('Accept', 'application/json')
						.then((response) => {
							expect(response.statusCode).to.equal(200);
							const blogs = response.body;
							expect(blogs).to.have.length(1);
							done();
						});
				});
		});
	});
});
