const sqlite3 = require('sqlite3').verbose();
new sqlite3.Database(':memory:');

require('../app/server/database/database');
require('../app/server/security/passport');
require('../app/server/express');
