# harbortouch-test

## Prerequisites
* Node + NPM
* PostgresSQL running on `192.168.99.100:5432` - can be configured by setting environment variables DATABASE_HOST and DATABASE_PORT or in app/server/config/config.json 

## Running for development
* run `npm install` in the application folder
* run `npm web-app-dev` and `npm server-dev` in separate terminal windows

The server will launch in dev mode on `localhost:3000`

The web-app will launch in dev mode on `localhost:4200` and will proxy all ajax requests made to `localhost:3000`

# Running
Run `npm start`

The server will start in production mode on `localhost:3000` and serve the bundled web-app

# Running with Docker
Run `docker-compose build` in the projects folder
Run `docker-compose up` after the image is built
The application should be running on `<docker-machine-ip>:3000`

## Test
The tests use an embedded SQLite database

Run `npm test` to run the server tests
