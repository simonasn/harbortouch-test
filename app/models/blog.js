import DS from 'ember-data';

export default DS.Model.extend({
	subject: DS.attr('string'),
	text: DS.attr('string'),
	userId: DS.attr('string'),
});
