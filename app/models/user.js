import DS from 'ember-data';

export default DS.Model.extend({
	username: DS.attr('string'),
	name: DS.attr('string'),
	lastname: DS.attr('string'),
	address: DS.attr('string'),
});
