import Ember from 'ember';
import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
	ajax: Ember.inject.service(),
	queryRecord() {
		const authorizer = this.get('authorizer');

		return new Ember.RSVP.Promise((resolve, reject) => {
			this.get('session').authorize(authorizer, (headerName, headerValue) => {
				this.get('ajax').request('/api/users/current', {
					method: 'GET',
					headers: {
						[headerName]: headerValue,
					},
				}).then((user) => resolve(user))
					.catch((error) => reject(error));
			});
		});
	},
	updateRecord(store, type, snapshot) {
		const authorizer = this.get('authorizer');
		let data = this.serialize(snapshot, { includeId: true });
		let id = snapshot.id;

		return new Ember.RSVP.Promise((resolve, reject) => {
			this.get('session').authorize(authorizer, (headerName, headerValue) => {
				this.get('ajax').request(`/api/users/${id}`, {
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						[headerName]: headerValue,
					},
					data: data,
					dataType: 'json',
				}).then((user) => resolve(user))
					.catch((error) => reject(error));
			});
		});
	},
});
