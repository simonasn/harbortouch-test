import Ember from 'ember';
import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Route.extend(ApplicationRouteMixin, {
	session: Ember.inject.service('session'),
	actions: {
		invalidateSession() {
			this.get('session').invalidate();
		}
	}
});
