import Route from '@ember/routing/route';

export default Route.extend({
	model() {
		return {
			blogs: this.store.findAll('blog'),
			user: this.store.queryRecord('user', {}),
		};
	},
});
