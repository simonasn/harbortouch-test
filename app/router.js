import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
	location: config.locationType,
	rootURL: config.rootURL
});

Router.map(function () {
	this.route('blogs', function () {
		this.route('new');
		this.route('edit', {path: ':blog_id/edit'});
	});
	this.route('profile');
	this.route('login');
	this.route('signup');
});

export default Router;
