const Sequelize = require('sequelize');
const {
	database: {
		name,
		username,
		password,
		host,
		port,
		dialect,
		pool,
		schema,
	}
} = require('../config/config');

const sequelize = new Sequelize(name, username, password, {
	host: process.env.DATABASE_HOST || host,
	port: process.env.DATABASE_PORT || port,
	dialect,
	schema,
	pool,
});

module.exports = sequelize;
