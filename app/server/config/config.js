const prodConfig = require('./config-prod.json');
const devConfig = require('./config.json');
const testConfig = require('./config-test.json');

let configToExport;

if (process.env.NODE_ENV === 'production') {
	configToExport = prodConfig;
} else if (process.env.NODE_ENV === 'test') {
	configToExport = testConfig;
} else {
	configToExport = devConfig;
}

module.exports = configToExport;
