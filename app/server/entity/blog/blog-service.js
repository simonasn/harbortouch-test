const { Blog } = require('../entities');

/**
 * Find all blogs
 *
 * @returns {Promise} A promise which resolves into an array of blogs
 */
function getAllBlogs() {
	return Blog.findAll({
		order: ['id'],
	});
}

/**
 * Find a blog by its id
 *
 * @param {number} id The id of the blog to find
 * @returns {Promise} A promise which resolves into a blog
 */
function findBlogById(id) {
	return Blog.findById(id);
}

/**
 * Saves the blog
 *
 * @param {Object} blogToSave blog to save
 * @returns {Promise} A promise which resolves into the saved blog
 */
function save(blogToSave) {
	return Blog.create(blogToSave).then((blog) => {
		return blog.save({ fields: ['subject', 'text', 'userId'] });
	});
}

/**
 * Updates the blog
 *
 * @param {number} id the id of the blog to update
 * @param {Object} blogData the data to update the blog with
 * @returns {Promise} A promise which resolve into the updated blog
 */
function update(id, blogData) {
	return findBlogById(id)
		.then((blog) => {
			return blog.update(blogData, { fields: ['subject', 'text'] });
		});
}

/**
 * Deletes the blog by id
 *
 * @param {number} id the id of the blog to delete
 * @returns {Promise} a promise which resolves when a blog is deleted
 */
function deleteById(id) {
	return findBlogById(id).then((blog) => {
		return blog.destroy();
	});
}

/**
 * Checks whether the user with userId can edit the blogId blog
 *
 * @param {number} userId the id of the user to check against
 * @param {number} blogId the id of the blog to check
 * @returns {Promise} a promise which resolves into true if the user can edit the blog, false otherwise
 */
function canUserEditBlog(userId, blogId) {
	return findBlogById(blogId).then((blog) => {
		return blog.userId === userId;
	});
}

/**
 * Deletes all blog entries
 *
 * @returns {Promise} a promise which resolves when the blogs are deleted
 */
function deleteAll() {
	return Blog.destroy({ where: {} });
}

module.exports = {
	getAllBlogs,
	findBlogById,
	save,
	update,
	deleteById,
	canUserEditBlog,
	deleteAll,
};
