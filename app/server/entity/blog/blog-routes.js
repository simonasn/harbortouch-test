const express = require('express');
const router = express.Router();

const blogService = require('./blog-service');
const { requireAuth } = require('../../security/passport-strategies');

router.get('/', (request, response) => {
	blogService.getAllBlogs()
		.then((blogs) => response.send(blogs))
		.catch((error) => response.status(500).send(error));
});

router.get('/:id', (request, response) => {
	const id = parseInt(request.params.id);

	blogService.findBlogById(id)
		.then((blog) => response.send(blog))
		.catch((error) => response.status(500).send(error));
});

router.post('/', requireAuth, (request, response) => {
	const userId = request.user.id;
	const blog = Object.assign({}, request.body, { userId });

	blogService.save(blog)
		.then((savedBlog) => response.send(savedBlog))
		.catch((error) => response.status(500).send(error));
});

router.put('/:id', requireAuth, (request, response) => {
	const id = parseInt(request.params.id);
	const blogData = request.body;
	const user = request.user;

	blogService.canUserEditBlog(user.id, id).then((userCanEditBlog) => {
		if (userCanEditBlog) {
			blogService.update(id, blogData)
				.then((updatedBlog) => response.send(updatedBlog))
				.catch((error) => response.status(500).send(error));
		} else {
			response.status(403).send({ error: 'The user cannot edit this blog' });
		}
	});
});

router.delete('/:id', requireAuth, (request, response) => {
	const id = parseInt(request.params.id);
	const user = request.user;

	blogService.canUserEditBlog(user.id, id).then((userCanEditBlog) => {
		if (userCanEditBlog) {
			blogService.deleteById(id)
				.then(() => response.status(204).send())
				.catch((error) => response.status(500).send(error));
		} else {
			response.status(403).send({ error: 'The user cannot delete this blog' });
		}
	});
});

module.exports = router;
