const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const sequelize = require('../database/database');

const User = sequelize.define('user', {
	username: {
		type: Sequelize.STRING
	},
	name: {
		type: Sequelize.STRING
	},
	lastname: {
		type: Sequelize.STRING
	},
	address: {
		type: Sequelize.STRING
	},
	password: {
		type: Sequelize.STRING,
	},
});

User.beforeCreate((user) => {
	return bcrypt.genSalt(10).then((salt) => {
		return bcrypt.hash(user.password, salt, null);
	}).then((hashedPassword) => {
		user.password = hashedPassword;
	});
});

User.prototype.toJSON = function () {
	const user = this.dataValues;
	delete user.password;
	return user;
};

User.prototype.comparePassword = function (candidatePassword, callback) {
	bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
		if (err) {
			return callback(err);
		}

		callback(null, isMatch);
	});
};

const Blog = sequelize.define('blog', {
	subject: {
		type: Sequelize.STRING
	},
	text: {
		type: Sequelize.STRING
	},
	userId: {
		type: Sequelize.INTEGER
	},
});

Blog.belongsTo(User, {
	foreignKey: 'userId',
	constraints: false,
	as: 'user'
});

User.sync();
Blog.sync();

module.exports = {
	Blog,
	User,
};
