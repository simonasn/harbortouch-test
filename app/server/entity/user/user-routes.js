const express = require('express');
const router = express.Router();
const { requireAuth } = require('../../security/passport-strategies');
const userService = require('./user-service');

router.get('/current', requireAuth, (request, response) => {
	const user = request.user;

	response.send(user);
});

router.put('/:id', requireAuth, (request, response) => {
	const id = parseInt(request.params.id);
	const userData = request.body;
	const user = request.user;

	if (user.id !== id) {
		response.status(403).send({ error: 'Cannot update profile - current user and the user to update do not match' });
	} else {
		userService.updateUser(id, userData)
			.then((updatedUser) => response.send(updatedUser))
			.catch((error) => response.status(500).send(error));
	}
});

module.exports = router;
