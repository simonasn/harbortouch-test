const { User } = require('../entities');

/**
 * Finds a user by its username
 *
 * @param {string} username the usersname by which to query for a user
 * @returns {Promise} a promise which resolves into a user
 */
function findUserByUsername(username) {
	return User.findOne({ where: { username } });
}

/**
 * Finds a user by its id
 *
 * @param {number} id the id to query the user by
 * @returns {Promise} a promise which resolves into a user
 */
function findById(id) {
	return User.findById(id);
}

/**
 * Creates a user
 *
 * @param {Object} userData the user to create
 * @returns {Promise} a promise which resolves into the saved user
 */
function createUser(userData) {
	return User.create(userData).then((user) => {
		return user.save({ fields: ['username', 'password'] });
	});
}

/**
 * Update the id user
 *
 * @param {number} id the id of the user to update
 * @param {Object} userData the user data to update the user with
 * @returns {Promise} a promise which resolves into the updated user
 */
function updateUser(id, userData) {
	return findById(id)
		.then((user) => {
			return user.update(userData, { fields: ['name', 'lastname', 'address'] });
		});
}

/**
 * Deletes all users
 *
 * @returns {Promise} a promise which resolves when all of the users are deleted
 */
function deleteAll() {
	return User.destroy({ where: {} });
}

module.exports = {
	findUserByUsername,
	findById,
	createUser,
	updateUser,
	deleteAll,
};
