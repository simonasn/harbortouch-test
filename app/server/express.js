const express = require('express');
const bodyParser = require('body-parser');
const serveStatic = require('serve-static');
const app = express();
const blogs = require('./entity/blog/blog-routes');
const users = require('./entity/user/user-routes');
const authentication = require('./security/authentication-route');

const { port } = require('./config/config');

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use('/api/blogs', blogs);
app.use('/api/users', users);
app.use('/api', authentication);
app.use('/', serveStatic('dist'));
app.use('*', serveStatic('dist'));

app.listen(port, () => {
	console.log(`Example app listening on port ${port}!`);
});

module.exports = app;
