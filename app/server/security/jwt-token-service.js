const jwt = require('jwt-simple');
const { secret } = require('../config/config');

/**
 *
 * @param {Object} user a user object for which to generate the token
 * @returns {string} a JWT token for the user
 */
function createTokenForUser(user) {
	const timestamp = new Date().getTime();
	return jwt.encode({sub: user.id, iat: timestamp}, secret);
}

/**
 * Decodes a user token
 * @param {string} token a token to decode
 * @returns {*} a decoded user token
 */
function decodeUserToken(token) {
	return jwt.decode(token, secret);
}

module.exports = {
	createTokenForUser,
	decodeUserToken,
};
