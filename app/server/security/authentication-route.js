const express = require('express');

const UserService = require('../entity/user/user-service');
const JwtTokenService = require('./jwt-token-service');
const { requireSignin } = require('./passport-strategies');

const router = express.Router();

router.post('/authenticate', requireSignin, (req, res) => {
	const { user } = req;
	res.json({ token: JwtTokenService.createTokenForUser(user) });
});

router.post('/signup', (req, res, next) => {
	const { username, password } = req.body;

	if (!username || !password) {
		return res.status(422).send({ error: 'You must provide an username and a password' });
	}

	const lowercaseUsername = username.toLowerCase();

	UserService.findUserByUsername(lowercaseUsername)
		.then((existingUser) => {
			if (existingUser) {
				return res.status(422).send({ error: 'Username is in use' });
			}

			const user = {
				username: lowercaseUsername,
				password,
			};

			return UserService.createUser(user);
		})
		.then((user) => {
			res.send({ token: JwtTokenService.createTokenForUser(user) });
		})
		.catch((error) => next(error));
});

module.exports = router;
