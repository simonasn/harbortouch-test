const passport = require('passport');
const {Strategy: JwtStrategy, ExtractJwt} = require('passport-jwt');
const LocalStrategy = require('passport-local');
const UserService = require('../entity/user/user-service');
const {secret} = require('../config/config');

// Create local strategy
const localOptions = {usernameField: 'username'};
const localLogin = new LocalStrategy(localOptions, (username, password, done) => {
	const lowercaseUsername = username.toLowerCase();

	UserService.findUserByUsername(lowercaseUsername).then((user) => {
		if (!user) {
			return done(null, false);
		}

		user.comparePassword(password, (err, isMatch) => {
			if (err) {
				return done(err, false);
			}

			if (!isMatch) {
				return done(null, false);
			}

			return done(null, user);
		});
	})
		.catch((error) => done(error, false));
});

// Set up options for JWT Strategy
const jwtOptions = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken('authorization'),
	secretOrKey: secret,
};

// Create JWT Strategy
const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
	// See if the user ID in the payload exists in our database
	// If it does, call 'done' with that user
	// otherwise, call done without a user object
	const {sub: id} = payload;

	UserService.findById(id).then((user) => {
		if (user) {
			done(null, user);
		} else {
			done(null, false);
		}
	})
		.catch((error) => done(error, false));
});

// Tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);
