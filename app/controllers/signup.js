import Ember from 'ember';
import Controller from '@ember/controller';

export default Controller.extend({
	ajax: Ember.inject.service(),
	clearForm() {
		this.setProperties({
			errorMessage: '',
			username: '',
			password: '',
		});
	},
	actions: {
		signup() {
			const userData = this.getProperties('username', 'password');

			this.get('ajax').post('/api/signup', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				data: userData,
			}).then(() => {
				this.transitionToRoute('login');
			}).catch((error) => {
				this.set('errorMessage', error.payload.error);
			});
		},
	}
});
