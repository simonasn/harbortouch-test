import Ember from 'ember';
import Controller from '@ember/controller';

export default Controller.extend({
	session: Ember.inject.service('session'),
	actions: {
		deleteBlog(id) {
			this.store.findRecord('blog', id, { backgroundReload: false })
				.then((blog) => {
					blog.destroyRecord();
					this.transitionToRoute('blogs');
				});
		},
	},
});
