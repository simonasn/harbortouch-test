import Ember from 'ember';
import Controller from '@ember/controller';

export default Controller.extend({
	session: Ember.inject.service('session'),

	sessionData: Ember.computed('session.session.content.authenticated', function() {
		return JSON.stringify(this.get('session.session.content.authenticated'), null, '\t');
	}),
});
