import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		updateProfile() {
			const id = this.model.get('id');
			const name = this.model.get('name');
			const lastname = this.model.get('lastname');
			const address = this.model.get('address');

			this.get('store').findRecord('user', id).then((user) => {
				user.set('name', name);
				user.set('lastname', lastname);
				user.set('address', address);

				return user.save();
			});

		},
		resetProfile() {
			this.model.rollbackAttributes();
		},
	},
});
