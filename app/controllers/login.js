import Ember from 'ember';
import Controller from '@ember/controller';

export default Controller.extend({
	session: Ember.inject.service('session'),
	clearForm() {
		this.setProperties({
			errorMessage: '',
			identification: '',
			password: '',
		});
	},
	actions: {
		authenticate() {
			const credentials = this.getProperties('identification', 'password');
			const authenticator = 'authenticator:token';

			this.get('session').authenticate(authenticator, credentials)
				.catch(() => {
					this.set('errorMessage', 'Invalid Username or Password');
				});
		},
	},
});
