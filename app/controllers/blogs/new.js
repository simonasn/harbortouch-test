import Controller from '@ember/controller';

export default Controller.extend({
	clearForm() {
		this.setProperties({
			subject: '',
			text: '',
		});
	},
	actions: {
		createBlog() {
			const subject = this.get('subject');
			const text = this.get('text');

			const blogRecord = this.store.createRecord('blog', { subject, text });
			blogRecord.save()
				.then(() => {
					this.setProperties({ subject: '', text: '' });
					this.transitionToRoute('blogs');
				});
		},
	},
});
