import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		updateBlog() {
			const id = this.model.get('id');
			const subject = this.model.get('subject');
			const text = this.model.get('text');

			this.get('store').findRecord('blog', id).then((blog) => {
				blog.set('subject', subject);
				blog.set('text', text);

				return blog.save();
			}).then(() => {
				this.transitionToRoute('blogs');
			});
		},
	},
});
