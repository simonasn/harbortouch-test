import Component from '@ember/component';

export default Component.extend({
	actions: {
		deleteBlogEntry(id) {
			this.sendAction('deleteBlogEntry', id);
		},
	},
});
