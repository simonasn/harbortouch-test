FROM node:8.8.0

RUN npm install --g ember-cli

WORKDIR /usr/src/app

COPY . .

RUN npm install
RUN npm rebuild node-sass --force
RUN npm rebuild bcrypt --force --update-binary
RUN npm run build

COPY dist .
